#include <stdio.h>
#include <string.h>
#include <stdlib.h>
int main()
{
    FILE *p;
    char str[30], frase[] = "Este e um arquivo chamado: ", resposta[80];
    int i;
    /* Le um nome para o arquivo a ser aberto: */
    printf("\n\n Entre com um nome para o arquivo:\n");
    fgets(str,29,stdin);/* Usa fgets como se fosse gets */
    for(i=0; str[i]; i++)
        if(str[i]=='\n')
            str[i]=0; /* Elimina o \n da string lida */
    if (!(p = fopen(str,"w"))) /* Caso ocorra algum erro na abertura do arquivo..*/
    {
        printf("Erro! Impossivel abrir o arquivo!\n");
        exit(1);
    }
    fputs(frase, p);
    fputs(str,p);
    fclose(p);
    /* abre novamente e le */
    p = fopen(str,"r");
    fgets(resposta, 79, p);
    printf("\n\n%s\n", resposta);
    fclose(p);
    remove(str);
    return(0);
}